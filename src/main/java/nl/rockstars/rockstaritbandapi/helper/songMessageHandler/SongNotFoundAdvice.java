package nl.rockstars.rockstaritbandapi.helper.songMessageHandler;

import nl.rockstars.rockstaritbandapi.song.Song;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * the {@link Song} not found Exception handler helper
 */
@ControllerAdvice
public class SongNotFoundAdvice {

    @ResponseBody
    @ExceptionHandler(SongNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    String songNotFoundHandler(SongNotFoundException e){
        return e.getMessage();
    }
}
