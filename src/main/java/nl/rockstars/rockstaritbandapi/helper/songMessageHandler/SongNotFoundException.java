package nl.rockstars.rockstaritbandapi.helper.songMessageHandler;

/**
 * handler {@link SongNotFoundException} exception
 */
public class SongNotFoundException extends RuntimeException {

    /**
     * Constructor
     * @param id the given Id (in Long)
     */
    public SongNotFoundException( final Long id){
        super("could not find artist with ID: "+ id);
    }
}
