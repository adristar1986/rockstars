package nl.rockstars.rockstaritbandapi.helper.artistMessageHanler;

/**
 * handler {@link ArtistNotFoundException} exception
 */
public class ArtistNotFoundException extends RuntimeException {

    /**
     * Constructor
     *
     * @param id the given id (in Long)
     */
    public ArtistNotFoundException(final Long id) {
        super("could not find artist with ID: " + id);
    }
}
