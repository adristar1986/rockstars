package nl.rockstars.rockstaritbandapi.helper.artistMessageHanler;

import nl.rockstars.rockstaritbandapi.artist.Artist;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * The {@link Artist} not found Exception handler helper
 */
@ControllerAdvice
public class ArtistNotFoundAdvice {

    @ResponseBody
    @ExceptionHandler(ArtistNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    String artistNotFoundHandler(ArtistNotFoundException e){
        return e.getMessage();
    }
}
