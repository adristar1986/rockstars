package nl.rockstars.rockstaritbandapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class RockstarItBandApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(RockstarItBandApiApplication.class, args);
    }

}
