package nl.rockstars.rockstaritbandapi.core;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import nl.rockstars.rockstaritbandapi.artist.Artist;
import nl.rockstars.rockstaritbandapi.service.ArtistService;
import nl.rockstars.rockstaritbandapi.service.SongService;
import nl.rockstars.rockstaritbandapi.song.Song;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * The DataLoader class take care for filling the H2 database with the {@link Artist}
 * and {@link Song} by reading the two JSON FILE (Artist and Song) and save the
 * content.
 */
@Component
public class DatabaseLoader implements ApplicationRunner {

    private final ArtistService artistService;
    private final SongService songService;

    /**
     *  Default Constructor
     *
     * @param artistService the {@link ArtistService}
     * @param songService the {@link SongService}
     */
    @Autowired
    public DatabaseLoader(final ArtistService artistService, final SongService songService) {
        this.artistService = artistService;
        this.songService = songService;
    }


    /**
     * Filling the Database with the {@link Artist} and {@link Song}
     *
     * @param args .
     */
    @Override
    public void run(ApplicationArguments args) {
        ObjectMapper mapper = new ObjectMapper();

        TypeReference<List<Artist>> typeReferenceArtist = new TypeReference<List<Artist>>() {
        };
        TypeReference<List<Song>> typeReferenceSong = new TypeReference<List<Song>>() {
        };

        InputStream inputStreamArtist = TypeReference.class.getResourceAsStream("/json/artists.json");
        InputStream inputStreamSong = TypeReference.class.getResourceAsStream("/json/songs.json");

        try {
            List<Artist> artists = mapper.readValue(inputStreamArtist, typeReferenceArtist);
            List<Song> songs = mapper.readValue(inputStreamSong, typeReferenceSong);

            artistService.save(artists);
            songService.save(songs);

            System.out.println("Users Saved!");
        } catch (IOException e) {
            System.out.println("Unable to save users: " + e.getMessage());
        }
    }
}
