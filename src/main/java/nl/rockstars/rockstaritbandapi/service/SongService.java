package nl.rockstars.rockstaritbandapi.service;

import nl.rockstars.rockstaritbandapi.helper.songMessageHandler.SongNotFoundException;
import nl.rockstars.rockstaritbandapi.song.Song;
import nl.rockstars.rockstaritbandapi.song.SongRepository;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

/**
 * Song Service
 */
@Service
public class SongService {

    /** the {@link SongRepository} */
    private SongRepository songRepository;

    /**
     * Defalt Constructor
     *
     * @param songRepository {@link SongRepository}
     */
    public SongService(final SongRepository songRepository) {
        this.songRepository = songRepository;
    }

    /**
     * Return a list of all {@link Song}
     *
     * @return a list of all {@link Song}
     */
    public Iterable<Song> list() {
        return this.songRepository.findAll();
    }

    /**
     * return {@link Song} by given ID
     *
     * @param id the given id (in Long)
     * @return a {@link Song} by the given id
     */
    public Song findById(@PathVariable final Long id) {
        return this.songRepository.findById(id).orElseThrow(() -> new SongNotFoundException(id));
    }

    /**
     * Return a list of all {@link Song} by the given genre
     *
     * @param genre the given genre
     * @return a list of all {@link Song} by the given genre
     */
    public List<Song> findByGenre(@PathVariable("genre") String genre) {
        return this.songRepository.findByGenre(genre);
    }

    /**
     * Return a list of all {@link Song} by the given name
     *
     * @param name the given genre
     * @return a list of all {@link Song} by the given name
     */
    public List<Song> findByName(@PathVariable("name") String name) {
        return this.songRepository.findByName(name);
    }

    /**
     * Return a list of all {@link Song} by the given artist
     *
     * @param artist the given genre
     * @return a list of all {@link Song} by the given artist
     */
    public List<Song> findByArtist(@PathVariable("name") String artist) {
        return this.songRepository.findByArtist(artist);
    }

    /**
     * Save a {@link Song}
     *
     * @param song the given {@link Song}
     */
    public void save(final Song song) {
        this.songRepository.save(song);
    }

    /**
     * Save a list of {@link Song}
     *
     * @param songs the given {@link Song}'s
     */
    public void save(List<Song> songs) {
        this.songRepository.saveAll(songs);
    }

    /**
     * Update {@link Song} by the given id
     *
     * @param id the given id
     * @param song the given {@link Song}
     */
    public void update(@PathVariable final Long id, @RequestBody final Song song) {
        Song currentSong = findById(id);

        //verify that the Song id exist
        if (currentSong == null) {
            System.out.println("song not found");
        }else{
            currentSong.setName(song.getName());
            currentSong.setYear(song.getYear());
            currentSong.setArtist(song.getArtist());
            currentSong.setShortName(song.getShortName());
            currentSong.setBpm(song.getBpm());
            currentSong.setDuration(song.getDuration());
            currentSong.setGenre(song.getGenre());
            currentSong.setSpotifyId(song.getSpotifyId());
            currentSong.setAlbum(song.getAlbum());
            this.songRepository.save(currentSong);
        }
    }

    /**
     * Delete a {@link Song} by given id
     *
     * @param id the given id
     */
    public void delete(@PathVariable final Long id) {
        this.songRepository.deleteById(id);
    }
}
