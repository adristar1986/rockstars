package nl.rockstars.rockstaritbandapi.service;

import nl.rockstars.rockstaritbandapi.artist.Artist;
import nl.rockstars.rockstaritbandapi.artist.ArtistRepository;
import nl.rockstars.rockstaritbandapi.helper.artistMessageHanler.ArtistNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

/**
 * Artist service
 */
@Service
public class ArtistService {

    /** the {@link ArtistRepository} */
    private ArtistRepository artistRepository;

    /**
     * Default Constructor
     * @param artistRepository {@link ArtistRepository}
     */
    public ArtistService(final ArtistRepository artistRepository) {
        this.artistRepository = artistRepository;
    }

    /**
     * Return a list of all {@link Artist}
     *
     * @return a list of all {@link Artist}
     */
    public Iterable<Artist> listOfAllArtist() {
        return this.artistRepository.findAll();
    }

    /**
     * return {@link Artist} by given ID
     *
     * @param id the given id (in Long)
     * @return a {@link Artist} by the given id
     */
    public Artist findById(@PathVariable final Long id) {
        return this.artistRepository.findById(id).orElseThrow(() -> new ArtistNotFoundException(id));
    }

    /**
     * Return a list of {@link Artist} by the given name
     *
     * @param name the given name (in String)
     * @return a list of {@link Artist} by the given name
     */
    public Artist findByName(@PathVariable final String name) {
        return this.artistRepository.findByName(name);
    }

    /**
     * Save a {@link Artist}
     *
     * @param artist the given {@link Artist}
     */
    public void save(final Artist artist) {
        this.artistRepository.save(artist);
    }

    /**
     * Save a list of {@link Artist}'s
     *
     * @param artists the given {@link Artist}'s
     */
    public void save(final List<Artist> artists) {
        this.artistRepository.saveAll(artists);
    }

    /**
     * Update {@link Artist} by the given id
     *
     * @param id the given id
     * @param artist the given {@link Artist}
     */
    public void update(@PathVariable final Long id, @RequestBody final Artist artist) {
        Artist currentArtist = findById(id);

        //verify that the Artist id exist
        if (currentArtist == null) {
            System.out.println("artist not found");
        }else{
            currentArtist.setName(artist.getName());
            this.artistRepository.save(currentArtist);
        }
    }

    /**
     * Delete a {@link Artist} by given id
     *
     * @param id the given id
     */
    public void delete(@PathVariable final Long id) {
        this.artistRepository.deleteById(id);
    }


}
