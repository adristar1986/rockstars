package nl.rockstars.rockstaritbandapi.artist;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Artist Entity class
 */
@Entity
public class Artist {

    /** Artist Id */
    @Id
    private Long id;

    /** Artist Name */
    @NotNull
    @Column(unique = true)
    private String name;

    /**
     * Default constructor
     */
    protected Artist() {
    }

    /**
     * Constructor
     *
     * @param id artist ID
     * @param name artist Name
     */
    public Artist(final Long id, final String name) {
        this.id = id;
        this.name = name;
    }

    /**
     * Return a Artist ID
     * @return id of artist (in Long)
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Set Artist Id
     * @param id to be set.
     */
    public void setId(final Long id) {
        this.id = id;
    }

    /**
     * Return Artist Name
     * @return name of artist (in String)
     */
    public String getName() {
        return name;
    }

    /**
     * Set Artist Name
     * @param name to be set
     */
    public void setName(final String name) {
        this.name = name;
    }
}