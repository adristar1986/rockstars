package nl.rockstars.rockstaritbandapi.artist;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

/**
 * Artist Interface for generic CRUD operations on a repository for a specific type.
 */
public interface ArtistRepository extends CrudRepository<Artist, Long> {


    /**
     * Return a name of the {@link Artist} with the given name.
     *
     * @param name the given Artist name
     * @return A list {@link Artist} with the given name
     */
    Artist findByName(@Param("name") String name);
}
