package nl.rockstars.rockstaritbandapi.controller;

import nl.rockstars.rockstaritbandapi.artist.Artist;
import nl.rockstars.rockstaritbandapi.service.SongService;
import nl.rockstars.rockstaritbandapi.song.Song;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Controller class to store/create/update/delete an songs
 */
@RestController
@RequestMapping("/songs")
public class SongController {

    /** the {@link SongService} service */
    private SongService songService;

    /**
     * Default Constructor
     *
     * @param songService the {@link SongService}
     */
    public SongController(final SongService songService) {
        this.songService = songService;
    }

    /**
     * Return a list of all {@link Song}
     *
     * @return a list of all {@link Song}
     */
    @GetMapping("/listOfSong")
    public Iterable<Song> list() {
        return this.songService.list();
    }

    /**
     * Return a {@link Song} by  the given id
     *
     * @param id of the artist (in Long)
     * @return a {@link Song} by id
     */
    @GetMapping("/listOfSong/{id}")
    public Song findById(@PathVariable Long id) {
        return this.songService.findById(id);
    }

    /**
     * Return a song by given genre
     *
     * @param genre the given name (in String)
     */
    @GetMapping("/listOfGenre/{genre}")
    public Iterable<Song> findByGenre(@PathVariable String genre){
        return this.songService.findByGenre(genre);
    }

    /**
     * Return a {@link Song} by given name
     *
     * @param name the given name (in String)
     * @return a list of {@link Song} by given name
     */
    @GetMapping("/listOfName/{name}")
    public List<Song> findByName(@PathVariable String name){
        return this.songService.findByName(name);
    }

    /**
     * Return a {@link Song} by given artist
     *
     * @param artist the given artist (in String)
     * @return a list of {@link Song} by given artist
     */
    @GetMapping("/listOfArtist/{artist}")
    public List<Song> findByArtist(@PathVariable String artist){
        return this.songService.findByArtist(artist);
    }

    /**
     * Create a new {@link Song} and store it in DB
     * @param song the given {@link Song}
     */
    @PostMapping("/createSong")
    public void createArtist(@RequestBody final Song song) {
        songService.save(song);
    }

    /**
     * Create a list of {@link Song}
     *
     * @param songs the given {@link Song}'s
     */
    @PostMapping("/createSongs")
    public void createArtists(@RequestBody final List<Song> songs) {
        songService.save(songs);
    }

    /**
     *  Update a {@link Song} by given ID
     *
     * @param id the given id (in long)
     * @param song the given {@link Song}
     */
    @PostMapping("/updateSong/{id}")
    public void updateArtists(@PathVariable Long id, @RequestBody final Song song) {
        songService.update(id, song);
    }

    /**
     * Delete a {@link Artist} by given ID
     *
     * @param id the given id (in Long)
     */
    @PostMapping("/deleteSong/{id}")
    public void delete(@PathVariable Long id) {
        this.songService.delete(id);
    }
}
