package nl.rockstars.rockstaritbandapi.controller;

import nl.rockstars.rockstaritbandapi.artist.Artist;
import nl.rockstars.rockstaritbandapi.service.ArtistService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Controller class to store/create/update/delete an artists
 */
@RestController
@RequestMapping("/artists")
public class ArtistController {

    /** the {@link ArtistService} service */
    private ArtistService artistService;

    /**
     * Default constructor
     *
     * @param artistService the {@link ArtistService}
     */
    public ArtistController(final ArtistService artistService) {
        this.artistService = artistService;
    }

    /**
     * Return a list of all {@link Artist}
     *
     * @return a list of all {@link Artist}
     */
    @GetMapping("/listOfArtist")
    public Iterable<Artist> list() {
        return this.artistService.listOfAllArtist();
    }

    /**
     * Return a {@link Artist} by  the given id
     *
     * @param id of the artist (in Long)
     * @return a artist by id
     */
    @GetMapping("/listOfArtist/{id}")
    public Artist one(@PathVariable Long id) {
        return this.artistService.findById(id);
    }

    /**
     * Return a artist by given name
     *
     * @param name the given name (in String)
     */
    @GetMapping("/listOfArtistByName/{name}")
    public Artist one(@PathVariable String name) {
       return this.artistService.findByName(name);
    }

    /**
     * Create a new {@link Artist} and store it in DB
     * @param artist the given {@link Artist}
     */
    @PostMapping("/createArtist")
    public void createArtist(@RequestBody final Artist artist) {
        artistService.save(artist);
    }

    /**
     * Create a list of {@link Artist}
     *
     * @param artist the given {@link Artist}'s
     */
    @PostMapping("/createArtists")
    public void createArtists(@RequestBody final List<Artist> artist) {
        artistService.save(artist);
    }

    /**
     *  Update a {@link Artist} by given ID
     *
     * @param id the given id (in long)
     * @param artist the given {@link Artist}
     */
    @PostMapping("/updateArtists/{id}")
    public void updateArtists(@PathVariable Long id, @RequestBody final Artist artist) {
        artistService.update(id, artist);
    }

    /**
     * Delete a {@link Artist} by given ID
     *
     * @param id the given id (in Long)
     */
    @PostMapping("/deleteArtist/{id}")
    public void delete(@PathVariable Long id) {
        this.artistService.delete(id);
    }
}
