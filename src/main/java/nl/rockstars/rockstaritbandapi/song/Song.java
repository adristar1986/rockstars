package nl.rockstars.rockstaritbandapi.song;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Song Entity class
 */
@Entity
public class Song {
    /** the Song id */
    @Id
    private Long id;

    /** the song name */
    @NotNull
    private String name;

    /** the song year */
    private int year;

    /** the song artist */
    private String artist;

    /** the song short name */
    private String shortName;

    /** the song bpm */
    private int bpm;

    /** the song duration */
    private int duration;

    /** the song genre */
    private String genre;

    /** the song spotify di */
    private String spotifyId;

    /** the song album */
    private String album;

    /**
     *  Default Constructor
     */
    protected Song() {
    }

    /**
     *  Constructor
     * @param id song id
     * @param name song name
     * @param year song year
     * @param artist song artist
     * @param shortName song short name
     * @param bpm song bpm
     * @param duration song duration
     * @param genre song genre
     * @param spotifyId song spotify id
     * @param album song album
     */
    public Song(final Long id, final String name, final int year,
                final String artist, final String shortName, final int bpm,
                final int duration, final String genre, final String spotifyId,
                final String album) {
        this.id = id;
        this.name = name;
        this.year = year;
        this.artist = artist;
        this.shortName = shortName;
        this.bpm = bpm;
        this.duration = duration;
        this.genre = genre;
        this.spotifyId = spotifyId;
        this.album = album;
    }

    /**
     * Return the song id
     *
     * @return the id of the song (in Long)
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Return the song name
     *
     * @return the name of the song (in String)
     */
    public String getName() {
        return this.name;
    }

    /**
     * Set the name of the song
     *
     * @param name the given name (in String)
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * Return the song year
     *
     * @return the year of the song (in int)
     */
    public int getYear() {
        return this.year;
    }

    /**
     * set the year of the song
     *
     * @param year the given year (in int)
     */
    public void setYear(final int year) {
        this.year = year;
    }

    /**
     * Return the song artist
     *
     * @return the artist of the song (in String)
     */
    public String getArtist() {
        return this.artist;
    }

    /**
     * Set the Artist of the song
     *
     * @param artist the given artist (in String)
     */
    public void setArtist(final String artist) {
        this.artist = artist;
    }

    /**
     * Return short name of the song
     *
     * @return the short name of the song (in String)
     */
    public String getShortName() {
        return shortName;
    }

    /**
     * Set the short name of the song
     *
     * @param shortName the given short name (inn String)
     */
    public void setShortName(final String shortName) {
        this.shortName = shortName;
    }

    /**
     * Return the bpm of the song
     *
     * @return the bpm of the song (in int)
     */
    public int getBpm() {
        return bpm;
    }

    /**
     * Set thebpm of the song
     *
     * @param bpm the given bpm (in bpm)
     */
    public void setBpm(final int bpm) {
        this.bpm = bpm;
    }

    /**
     * Return the duration of the song
     *
     * @return the duration of the song (in int)
     */
    public int getDuration() {
        return duration;
    }

    /**
     * Set the duration of the song
     *
     * @param duration the given duration (in int)
     */
    public void setDuration(final int duration) {
        this.duration = duration;
    }

    /**
     * Return the genre of the song
     *
     * @return the genre of the song (in String)
     */
    public String getGenre() {
        return genre;
    }

    /**
     * Set the genre of the song
     * @param genre the given genre (in String)
     */
    public void setGenre(final String genre) {
        this.genre = genre;
    }

    /**
     * Return the Spotify ID of the song
     *
     * @return the spotify ID of the song (in String)
     */
    public String getSpotifyId() {
        return spotifyId;
    }

    /**
     * Set the Spotify ID of the song
     *
     * @param spotifyId the given Spotify ID (in String)
     */
    public void setSpotifyId(final String spotifyId) {
        this.spotifyId = spotifyId;
    }

    /**
     * Return the album of the song
     *
     * @return the album of the song (in String)
     */
    public String getAlbum() {
        return album;
    }

    /**
     * Set the album of the song
     *
     * @param album the given album (in Sting)
     */
    public void setAlbum(final String album) {
        this.album = album;
    }
}