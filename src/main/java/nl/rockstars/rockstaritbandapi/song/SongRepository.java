package nl.rockstars.rockstaritbandapi.song;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Song Interface for generic CRUD operations on a repository for a specific type.
 */
public interface SongRepository extends CrudRepository<Song, Long> {

    /**
     * Return a list of {@link Song} with the given name.
     *
     * @param name the given name
     *
     * @return a list of {@link Song} with the given name
     */
    List<Song> findByName(@Param("name") String name);

    /**
     * Return a list of {@link Song} with the given artist.
     *
     * @param artist the given artist
     *
     * @return a list of {@link Song} with the given artist
     */
    List<Song> findByArtist(@Param("artist") String artist);


    /**
     * Return a list of {@link Song} with the given genre.
     *
     * @param genre the given genre
     *
     * @return a list of {@link Song} with the given genre
     */
    List<Song> findByGenre(@Param("genre") String genre);
}
