# Rockstars IT Band API

## Importing a Getting Started guide
Unzip the project to your choices directory.
 
Import the project with Gradle.

Build the project with gradle.

## Start-up Application

To start the application just run ``RockstarItBandApiApplication.java``

## Test the service

Now that the service is up, visit http://localhost:8080/artists/listOfArtist

### Artist By ID
Get command: 
 http://localhost:8080/artists/listOfArtist/your_id
 
### Artist By Name
Get Command: 
 http://localhost:8080/artists/listOfArtist/1istOfArtistByName/your_name
 
### Create new Artist
 Post command:
 http://localhost:8080/artists/listOfArtist/createArtists
 
 ### Update Artist
 Post command:
  http://localhost:8080/artists/listOfArtist/updateArtists/your_id
  
 ### Delete Artist
 http://localhost:8080/artists/listOfArtist/deleteArtist/your_id
 